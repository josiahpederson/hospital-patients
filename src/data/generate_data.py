import datetime
import random


def generate_data():
    """
    A hard coded function to insert dates into a data set.
    """
    days = []
    for day in range(1, 6):
        for hour in range(8, 15, 2):
            days.append(
                datetime.datetime(
                    year=2022, month=9, day=day, hour=hour, minute=random.randint(0, 59)
                ).timestamp()
            )

    data = [
        {
            "hospitalId": "HOSP111",
            "patientId": "PAT111",
            "event": "Admission",
            "timestamp": days[0],
        },
        {
            "hospitalId": "HOSP111",
            "patientId": "PAT333",
            "event": "Admission",
            "timestamp": days[1],
        },
        {
            "hospitalId": "HOSP222",
            "patientId": "PAT222",
            "event": "Admission",
            "timestamp": days[2],
        },
        {
            "hospitalId": "HOSP111",
            "patientId": "PAT555",
            "event": "PhysicianVisit",
            "timestamp": days[3],
        },
        {
            "hospitalId": "HOSP111",
            "patientId": "PAT111",
            "event": "Discharge",
            "timestamp": days[4],
        },
        {
            "hospitalId": "HOSP111",
            "patientId": "PAT777",
            "event": "Admission",
            "timestamp": days[5],
        },
        {
            "hospitalId": "HOSP222",
            "patientId": "PAT222",
            "event": "Discharge",
            "timestamp": days[6],
        },
        {
            "hospitalId": "HOSP222",
            "patientId": "PAT444",
            "event": "PhysicianVisit",
            "timestamp": days[7],
        },
        {
            "hospitalId": "HOSP111",
            "patientId": "PAT111",
            "event": "Admission",
            "timestamp": days[8],
        },
        {
            "hospitalId": "HOSP111",
            "patientId": "PAT111",
            "event": "Discharge",
            "timestamp": days[9],
        },
        {
            "hospitalId": "HOSP111",
            "patientId": "PAT111",
            "event": "invalid_event",
            "timestamp": days[10],
        },
        {
            "hospitalId": "HOSP222",
            "patientId": "PAT666",
            "event": "Admission",
            "timestamp": days[10],
        },
    ]
    return data


if __name__ == "__main__":
    generate_data()
