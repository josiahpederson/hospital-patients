from data.saved_data import hospital_visits


def get_current_patient_total(hospital_id=None, hospital_visits=hospital_visits):
    """
    Returns the cumulative number of patients currently in all hospitals.
    If hospital_id is included in the args, this function will only return
    the total in the selected hospital.
    """
    if hospital_id is not None:
        try:
            data = {hospital_id: hospital_visits[hospital_id]["current_patient_total"]}
        except KeyError:
            data = {"message": "KeyError: Hospital not found"}
        return data
    total = []
    for hospital in hospital_visits:
        total.append({hospital: hospital_visits[hospital]["current_patient_total"]})
    return {"hospital_totals": total}
