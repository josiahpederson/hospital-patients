from datetime import datetime


def generate_friendly_date(date):
    try:
        return datetime.fromtimestamp(date).strftime("%d-%m-%y")
    except TypeError:
        raise TypeError("Timestamp must be of 'float' type.")
    except ValueError:
        raise ValueError("Timestamp is out of range.")


def convert_seconds_to_hours(seconds):
    hours = round(seconds / 60 / 60, 1)
    return str(hours) + " hours"
