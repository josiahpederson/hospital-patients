from data.saved_data import hospital_visits, physician_visits
from functions.acl_functions import (
    event_is_invalid,
    discharge_patient,
    admit_patient,
    add_hospital,
    add_patient,
)


def capture_data(
    patient_event, physician_visits=physician_visits, hospital_visits=hospital_visits
):
    """
    Incoming patient data is verified, reshaped and saved for further analysis.
    """
    invalid_event = event_is_invalid(patient_event)
    if invalid_event:
        return invalid_event

    if patient_event["event"] == "PhysicianVisit":
        patient_id = patient_event["patientId"]

        if not physician_visits.get(patient_id):
            add_patient(patient_id)

        physician_visits[patient_id]["visits_record"].append(
            {
                "date": patient_event["timestamp"],
                "dispatched_from": patient_event["hospitalId"],
            }
        )

        physician_visits[patient_id]["total_visits"] += 1

    elif patient_event["event"] == "Discharge":
        discharge_patient(patient_event)

    elif patient_event["event"] == "Admission":
        hospital_id = patient_event["hospitalId"]
        if not hospital_visits.get(hospital_id):
            add_hospital(hospital_id)
        admit_patient(patient_event)
