hospital_visits = {
    "HOSP111": {
        "patient_records": {
            "PAT111": [
                {"admission_date": 1662037560.0, "discharge_date": 1662124380.0},
                {"admission_date": 1662210360.0, "discharge_date": 1662218580.0},
            ],
            "PAT333": [{"admission_date": 1662047040.0, "discharge_date": None}],
            "PAT777": [{"admission_date": 1662130920.0, "discharge_date": None}],
        },
        "current_patient_total": 2,
    },
    "HOSP222": {
        "patient_records": {
            "PAT222": [
                {"admission_date": 1662053220.0, "discharge_date": 1662141120.0}
            ],
            "PAT666": [{"admission_date": 1662224820.0, "discharge_date": None}],
        },
        "current_patient_total": 1,
    },
}

physician_visits = {
    "PAT555": {
        "visits_record": [{"date": 1662061860.0, "dispatched_from": "HOSP111"}],
        "total_visits": 1,
    },
    "PAT444": {
        "visits_record": [{"date": 1662147360.0, "dispatched_from": "HOSP222"}],
        "total_visits": 1,
    },
}
