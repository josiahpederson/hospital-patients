# Hospital Patients
A stream of patient records is sent to this application which reshapes the data for further analysis. An example set of methods are available to interact with the data.

## Getting Started
1. Clone the repo.
2. This is a containerized project. Install and open [Docker Desktop](https://www.docker.com/products/docker-desktop/). If you prefer to use a `venv`, follow [the instructions for Docker haters](#instructions-for-docker-haters).
3. From the project root directory, initialize repo with the terminal command `./initialize`. This will build a docker image and run a container with all libraries installed. This is a development container and changes made in the `src` python modules will be reflected in real time.
4. From the root directory, start a brief simulation of the application receiving patient data and its methods with command: `./intake`.
5. Explore the docs to learn more about the application.

## Docs
* Learn about the structure of the patient records before and after being processed by this application [here](docs/data-structure.md).
* Learn about how to run built-in unit tests [here](docs/testing.md).
* Learn about this application's methods [here](docs/methods.md).

## Instructions for Docker haters
1. Clone the repo.
2. In the terminal, navigate to the `src` folder.
3. Create a virtual environment and activate it.
4. Use command `python -m pip install requirements.txt` to install libraries.
5. From the src directory, run the command `python main.py` to run a brief simulation of the application receiving patient data and its methods.
