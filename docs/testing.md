# Testing
This application comes with a suite of unit tests that ensure all its methods work properly.

### Running unit tests (with Docker)
* From the `root directory`, run the command `./test` to run all the tests.
* Alternatively, you can run specific tests from the `root directory` terminal with command `docker compose exec hospital python -m pytest -vv tests/desired_test.py`.

### Running unit tests (without Docker)
* Make sure your `venv` is activated.
* Navigate to the `src` directory and run terminal command `python -m pytest -vv tests` to run all the tests.
* To run individual tests, run command `python -m pytest -vv tests/desired_test.py`.
