from functions.common.time_conversion import generate_friendly_date
from unittest import TestCase


class TestGenerateFriendlyDate(TestCase):
    def test_generates_correct_format(self):
        timestamp = 1662053220.0
        results = generate_friendly_date(timestamp)
        expected_results = "01-09-22"
        assert results == expected_results

    def test_incorrect_input(self):
        with self.assertRaises(ValueError):
            generate_friendly_date(12345678790125123)
        with self.assertRaises(TypeError):
            generate_friendly_date("string")
