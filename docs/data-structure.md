# Data structure

## Input recurds
The structure of a patient record passed into this application is as follows:
```python
    {
        "hospitalId": str,
        "patientId": str,
        "event": str,
        "timestamp": float,
    }
```
Here is an example with data filled out.
```python
{
    "hospitalId": "HOSP111",
    "patientId": "PAT111",
    "event": "Admission",
    "timestamp": 1662053220.0,
}
```
Valid `events` are "Admission", "Discharge", and "physicianVisit".

## Cleaned records
The structure of the data after being cleaned is:
```python
    hospital_visits = {
        "HOSPITALID": {
            "patient_records": {
                "PATIENTID": [
                    {"admission_date": float, "discharge_date": float},
                    {"admission_date": float, "discharge_date": float or None},
                ],
                # Repeats for each patient
            },
            "current_patient_total": int
        },
        # Repeats for each hospital
    }

    physician_visits = {
        "PATIENTID": {
            "visits_record": [{"date": float, "dispatched_from": "HOSPITALID"}],
            "total_visits": int,
        },
        # Repeats for each patient
    }
```


Here is an example with data filled out.
```python
hospital_visits = {
    "HOSP111": {
        "patient_records": {
            "PAT111": [
                {"admission_date": 1662037560.0, "discharge_date": 1662124380.0},
                {"admission_date": 1662210360.0, "discharge_date": 1662218580.0},
            ],
            "PAT333": [{"admission_date": 1662047040.0, "discharge_date": None}],
            "PAT777": [{"admission_date": 1662130920.0, "discharge_date": None}],
        },
        "current_patient_total": 2,
    },
    "HOSP222": {
        "patient_records": {
            "PAT222": [
                {"admission_date": 1662053220.0, "discharge_date": 1662141120.0}
            ],
            "PAT666": [{"admission_date": 1662224820.0, "discharge_date": None}],
        },
        "current_patient_total": 1,
    },
}

physician_visits = {
    "PAT555": {
        "visits_record": [{"date": 1662061860.0, "dispatched_from": "HOSP111"}],
        "total_visits": 1,
    },
    "PAT444": {
        "visits_record": [{"date": 1662147360.0, "dispatched_from": "HOSP222"}],
        "total_visits": 1,
    },
}

```
