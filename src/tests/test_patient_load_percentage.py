from data.test_data import hospital_visits
from functions.patient_load_percentage import get_patient_load_percentage


def test_patient_load_percentage_with_correct_data():
    expected_output = [{"HOSP111": 66.67}, {"HOSP222": 33.33}]
    results = get_patient_load_percentage(hospital_visits)
    assert results == expected_output


def test_patient_load_percentage_with_no_data():
    expected_output = "No patient records."
    results = get_patient_load_percentage({})
    assert results == expected_output


def test_patient_load_percentage_non_iterable_type():
    expected_output = "TypeError: Argument must be of following types: 'dict', 'list'"
    results = get_patient_load_percentage(123)
    assert results == expected_output


def test_patient_load_percentage_wrong_keys():
    expected_output = "KeyError: Argument is missing necessary keys."
    results = get_patient_load_percentage(
        {"a_key": {"that_is_wrong": "will_fail_gracefully"}}
    )
    assert results == expected_output
