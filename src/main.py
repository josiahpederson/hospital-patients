from functions.number_of_physician_visits import (
    get_number_of_physician_visits,
)
from functions.current_patient_total import get_current_patient_total
from functions.total_admissions import get_total_admissions
from functions.patient_load_percentage import get_patient_load_percentage
from functions.cumulative_patient_hours import get_cumulative_patient_hours
from functions.length_of_stay import get_length_of_stay
from functions.intake import capture_data
from data.generate_data import generate_data
import pprint, time


data = generate_data()
pp = pprint.PrettyPrinter(indent=1)

for e in data:
    time.sleep(1)
    print(f"receiving {e}")
    capture_data(e)

time.sleep(1)
print('Following are the results of example methods being run on the records just received.')
time.sleep(3)

pp.pprint({"get_length_of_stay": get_length_of_stay("HOSP111", "PAT111", many=True)})
time.sleep(1)

pp.pprint({"get_current_patient_total": get_current_patient_total("HOSP111")})
time.sleep(1)
pp.pprint({"get_total_admissions": get_total_admissions("HOSP111")})
time.sleep(1)
pp.pprint(
    {
        "get_number_of_physician_visits": get_number_of_physician_visits(
            "12/9/2020", "12/12/2022"
        )
    }
)
time.sleep(1)
pp.pprint({"get_patient_load_percentage": get_patient_load_percentage()})
time.sleep(1)
pp.pprint({"get_cumulative_patient_hours": get_cumulative_patient_hours()})
time.sleep(2)
print("thanks for watching!")
