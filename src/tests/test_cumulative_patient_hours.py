from functions.cumulative_patient_hours import get_cumulative_patient_hours
from unittest import TestCase
from data.test_data import hospital_visits


class TestCumulativePatientHours(TestCase):
    def test_cumulative_patient_hours_correct_data(self):
        results = get_cumulative_patient_hours(hospital_visits=hospital_visits)
        self.assertEqual(type(results), str)
        hours_int = float(results.split(" ")[0])
        assert hours_int > 1

    def test_cumulative_patient_hours_invalid_data(self):
        with self.assertRaises(TypeError):
            get_cumulative_patient_hours(None)
            get_cumulative_patient_hours({"invalid_key": "whoops"})
