from functions.common.time_conversion import convert_seconds_to_hours
from unittest import TestCase


class TestTimeConversion(TestCase):
    def test_conversion_with_correct_data(self):
        seconds = 82450
        results = convert_seconds_to_hours(seconds)
        expected_results = "22.9 hours"
        assert results == expected_results

    def test_incorrect_inputs(self):
        with self.assertRaises(TypeError):
            convert_seconds_to_hours("string")
        with self.assertRaises(TypeError):
            convert_seconds_to_hours()
        with self.assertRaises(TypeError):
            convert_seconds_to_hours({})
