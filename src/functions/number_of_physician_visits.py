from data.saved_data import (
    physician_visits,
)
from datetime import datetime
from .common.time_conversion import (
    generate_friendly_date,
)


def calculate_number_of_visits(
    start_date, end_date, patient_id, physician_visits=physician_visits
):
    """
    Returns a patient's records for physician visits within the specified date range
    """
    try:
        visits = physician_visits[patient_id]["visits_record"]
    except KeyError:
        return None
    matching_visits = []
    for visit in visits:

        if start_date <= visit["date"] <= end_date:
            date = generate_friendly_date(visit["date"])
            matching_visits.append(
                {"date": date, "dispatched_from": visit["dispatched_from"]}
            )
    return matching_visits


def get_number_of_physician_visits(
    start_str, end_str, patient_id=None, physician_visits=physician_visits
):
    """
    Totals all physician_visits within specified date range.
    If a patient_id is included in args, only that patient's records will be searched.
    Returns the total and a list of dictionaries with additional info about
    each visit.
    """
    format_str = "%d/%m/%Y"
    start_date = datetime.strptime(start_str, format_str)
    end_date = datetime.strptime(end_str, format_str)

    if patient_id is not None:
        visits = calculate_number_of_visits(
            start_date.timestamp(), end_date.timestamp(), patient_id
        )
        if visits is None:
            return {"message": "No physican visits found for this patient ID."}
        total = len(visits)

    else:
        total = 0
        visits = []
        for patient in physician_visits:
            selected_visits = calculate_number_of_visits(
                start_date.timestamp(), end_date.timestamp(), patient
            )
            visits += selected_visits
            total += len(selected_visits)
    return total, visits
