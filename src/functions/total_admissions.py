from data.saved_data import hospital_visits


def calculate_total_admissions(hospital_id, hospital_visits=hospital_visits):
    """
    Calculates the total visits for a given hospital.
    """
    try:
        data = len(hospital_visits[hospital_id]["patient_records"].keys())
    except KeyError:
        data = {"message": "KeyError: Hospital not found"}
    except TypeError:
        data = {"message": "TypeError: Unable to calculate number of admissions."}
    return data


def get_total_admissions(hospital_id=None, hospital_visits=hospital_visits):
    """
    Calculates the total number of patients each hospital has ever had.
    If a hospital_id arg is provided, only the total for that hospital will be returned.
    """
    if hospital_id is not None:
        data = calculate_total_admissions(hospital_id, hospital_visits)
        if type(data) == int:
            return {"total": data}
        else:
            return data
    else:
        total = 0
        for hospital in hospital_visits:
            data = calculate_total_admissions(hospital, hospital_visits)
            if type(data) == int:
                total += data
            else:
                return data
        return {"total": total}
