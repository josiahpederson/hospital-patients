from functions.intake import capture_data
from data.saved_data import hospital_visits, physician_visits


def run_function_with_missing_keys(key):
    data = {
        "hospitalId": "HOSP111",
        "patientId": "PAT111",
        "event": "Admission",
        "timestamp": 1662225420.0,
    }
    del data[key]
    return capture_data(data)


def run_function_on_incorrect_values(bad_value, target_field):
    data = {
        "hospitalId": "HOSP111",
        "patientId": "PAT111",
        "event": "Admission",
        "timestamp": 1662225420.0,
    }
    data[target_field] = bad_value
    return capture_data(data)


def test_Admission_and_Discharge_with_correct_data():
    admission = {
        "hospitalId": "HOSP111",
        "patientId": "PAT111",
        "event": "Admission",
        "timestamp": 1662225420.0,
    }
    discharge = {
        "hospitalId": "HOSP111",
        "patientId": "PAT111",
        "event": "Discharge",
        "timestamp": 1662236425.1,
    }
    expected_output = {
        "HOSP111": {
            "patient_records": {
                "PAT111": [
                    {"admission_date": 1662225420.0, "discharge_date": 1662236425.1}
                ]
            },
            "current_patient_total": 0,
        }
    }
    capture_data(admission)
    capture_data(discharge)
    assert hospital_visits == expected_output


def test_physician_visit_intake_with_correct_data():
    admission = {
        "hospitalId": "HOSP111",
        "patientId": "PAT555",
        "event": "PhysicianVisit",
        "timestamp": 1662236425.1,
    }
    expected_output = {
        "PAT555": {
            "visits_record": [{"date": 1662236425.1, "dispatched_from": "HOSP111"}],
            "total_visits": 1,
        }
    }
    capture_data(admission)
    assert physician_visits == expected_output


def test_intake_with_incorrect_timestamp_type():
    types = [None, "string", 123, {}, []]
    for type in types:
        results = run_function_on_incorrect_values(type, "timestamp")
        expected_output = "ERROR: Check input types: timestamp:float, event:str, hospitalId:str, patientId:str"
        assert results is not None
        assert results == expected_output


def test_intake_with_incorrect_hospitalId_type():
    types = [None, 123.4, 123, {}, []]
    for type in types:
        expected_output = "ERROR: Check input types: timestamp:float, event:str, hospitalId:str, patientId:str"
        results = run_function_on_incorrect_values(type, "hospitalId")
        assert results is not None
        assert results == expected_output


def test_intake_with_incorrect_event_type():
    types = [None, 123.4, 123, {}, []]
    for type in types:
        expected_output = "ERROR: Check input types: timestamp:float, event:str, hospitalId:str, patientId:str"
        results = run_function_on_incorrect_values(type, "event")
        assert results is not None
        assert results == expected_output


def test_intake_with_incorrect_patientId_type():
    types = [None, 123.4, 123, {}, []]
    for type in types:
        expected_output = "ERROR: Check input types: timestamp:float, event:str, hospitalId:str, patientId:str"
        results = run_function_on_incorrect_values(type, "patientId")
        assert results is not None
        assert results == expected_output


def test_timestamp_is_too_large():
    timestamp = 123455678901234567890.6
    data = {
        "hospitalId": "HOSP111",
        "patientId": "PAT111",
        "event": "Admission",
        "timestamp": timestamp,
    }
    expected_output = f"ERROR: timestamp: {timestamp} is is too large."
    results = capture_data(data)
    assert results is not None
    assert results == expected_output


def test_missing_input_data():
    expected_output = "ERROR: Missing one or more key - value pairs. Required: timestamp, event, hospitalId, patientId"
    keys = ["hospitalId", "patientId", "event", "timestamp"]
    for key in keys:
        results = run_function_with_missing_keys(key)
        assert results is not None
        assert results == expected_output


def test_intake_returns_none():
    data = {
        "hospitalId": "HOSP111",
        "patientId": "PAT111",
        "event": "Admission",
        "timestamp": 1662225420.0,
    }
    assert capture_data(data) == None
