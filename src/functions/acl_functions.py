from data.saved_data import hospital_visits, physician_visits
import datetime


def event_is_invalid(patient_event):
    """
    Ensures data sent to capture_data is in the correct shape.
    Returns None if there are no issues. Returns a string if there are issues.
    """
    valid_events = ["PhysicianVisit", "Discharge", "Admission"]
    try:
        patient_event["timestamp"]
        patient_event["event"]
        patient_event["hospitalId"]
        patient_event["patientId"]
    except KeyError:
        return "ERROR: Missing one or more key - value pairs. Required: timestamp, event, hospitalId, patientId"

    if not all(
        [
            isinstance(patient_event.get("timestamp"), float),
            isinstance(patient_event.get("event"), str),
            isinstance(patient_event.get("hospitalId"), str),
            isinstance(patient_event.get("patientId"), str),
        ]
    ):
        return "ERROR: Check input types: timestamp:float, event:str, hospitalId:str, patientId:str"

    try:
        datetime.datetime.fromtimestamp(patient_event["timestamp"])
    except OverflowError:
        return f"ERROR: timestamp: {patient_event['timestamp']} is is too large."

    if not patient_event.get("event") in valid_events:
        return f"ERROR: '{patient_event['event']}' is an invalid event."


def discharge_patient(patient_event, hospital_visits=hospital_visits):
    """
    Handles updating an existing hospital admission with the discharge date
    """
    hospital_id = patient_event["hospitalId"]
    patient_id = patient_event["patientId"]
    discharge_date = patient_event["timestamp"]

    hospital_visits[hospital_id]["patient_records"][patient_id][-1][
        "discharge_date"
    ] = discharge_date
    hospital_visits[hospital_id]["current_patient_total"] -= 1


def admit_patient(patient_event, hospital_visits=hospital_visits):
    """
    Handles recording the admission of a new or existing patient to a hospital
    """
    hospital_id = patient_event["hospitalId"]
    patient_id = patient_event["patientId"]

    if not hospital_visits[hospital_id]["patient_records"].get(patient_id):
        hospital_visits[hospital_id]["patient_records"][patient_id] = []

    hospital_visits[hospital_id]["patient_records"][patient_id].append(
        {
            "admission_date": patient_event["timestamp"],
            "discharge_date": None,
        }
    )
    hospital_visits[hospital_id]["current_patient_total"] += 1


def add_hospital(hospital_id, hospital_visits=hospital_visits):
    """
    Adds a new hospital to the existing records.
    """
    hospital_visits[hospital_id] = {
        "patient_records": {},
        "current_patient_total": 0,
    }


def add_patient(patient_id, physician_visits=physician_visits):
    """
    This adds a new patient to the physician_visit records
    """
    physician_visits[patient_id] = {
        "visits_record": [],
        "total_visits": 0,
    }
