from data.saved_data import hospital_visits


def get_patient_load_percentage(hospital_visits=hospital_visits):
    """
    Compares current number of patients in each hospital and returns the percentage
    load each bears.
    """
    total_patients = 0
    hospitals_list = []
    try:
        for hospital in hospital_visits:
            current_hospital_total = hospital_visits[hospital]["current_patient_total"]
            total_patients += current_hospital_total
            hospitals_list.append(
                {
                    "name": hospital,
                    "total": current_hospital_total,
                }
            )
    except TypeError:
        return "TypeError: Argument must be of following types: 'dict', 'list'"
    except KeyError:
        return "KeyError: Argument is missing necessary keys."

    for hospital in hospitals_list:
        hospital_name = hospital["name"]
        percentage = (hospital["total"] / total_patients) * 100
        hospital[hospital_name] = round(percentage, 2)
        del hospital["name"]
        del hospital["total"]
    return hospitals_list if len(hospitals_list) > 0 else "No patient records."
