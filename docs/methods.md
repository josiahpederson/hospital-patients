# Methods
This application has a built-in suite of methods for interacting with the patient data. This page lists them and gives a brief explanation of what each does. (Note: There are a few ACL and helper functions that are not included in this documentation but each has a docstring if you would like to learn more.)


### capture_data(patient_event, physician_visits, hospital_visits)
This method is in charge of receiving patientEvents from outside the application. It restructures the data and saves it in the `hospital_visits` and `physician_visits` records.
* Argument: `patient_event` dictionary
* Argument: `physician_visits` dictionary
* Argument: `hospital_visits` dictionary
* Returns: None unless the data was invalid. In that case, it returns a message.
* See the code [here](../src/functions/intake.py)


### get_patient_load_percentage(hospital_visits)
This method compares the current number of patients in each hospital and returns the percentage load each bears.
* Argument: `hospital_visits` dictionary
* Returns: List of key-value pairs: "HOSPITALID': percentage ie: `[{'hosp111': 25}, {'hosp222': 75}]`
* See the code [here](../src/functions/patient_load_percentage.py)


### get_length_of_stay(hospital_id, patient_id, many, hospital_visits)
This method finds a desired patient record and returns the duration of one of their visits. If the `many` boolean is set to `True`, it returns the duration of all their visits.
* Argument: `hospital_id` string
* Argument: `patient_id` string
* Argument: `many` boolean
* Argument: `hospital_visits` dictionary
* Returns: Dictionarly containing visit dates and duration or list of dictionaries containing dates and duration.
* See the code [here](../src/functions/length_of_stay.py)


### get_total_admissions(hospital_id, hospital_visits)
This method calculates the total number of patients each hospital has ever had. If a `hospital_id` arg is provided, only the total for that hospital will be returned.
* Argument: `hospital_id` string
* Argument: `hospital_visits` dictionary
* Returns: a dictionary with the total visits in this format `{'total': 25}`
* See the code [here](../src/functions/total_admissions.py)


### get_number_of_physician_visits(start_str, end_str, patient_id, physician_visits)
This method totals all `physician_visits` within specified date range. If a `patient_id` is included in `args`, only that patient's records will be searched. Returns the total and a list of dictionaries with additional info about each visit.
* Argument: `start_str` string formatted date: 23/10/2022
* Argument: `end_str` string formatted date: 24/10/2022
* Argument: `patient_id` string
* Argument: `physician_visits` dictionary
* Returns: `total` integer
* Returns: `visits` dictionary
* See the code [here](../src/functions/number_of_physician_visits.py)


### get_current_patient_total(hospital_id, hospital_visits)
This method gets the current number of patients in all hospitals. If you pass in the optional `hospital_id` argument, only the current total of that hospital will be returned.
* Argument: `hospital_id` string (optional)
* Argument: `hospital_visits` dictionary
* Returns: Total for single hospital or list of totals for each hospital
* See the code [here](../src/functions/current_patient_total.py)


### get_cumulative_patient_hours(hospital_visits)
This method is a fun one that counts the cumulative number of hours all patients have spent in all the hospitals up unitl now.
* Argument: `hospital_visits` dictionary
* Returns: a string in the format "1234.5 hours"
* See the code [here](../src/functions/cumulative_patient_hours.py)
