from .common.time_conversion import (
    generate_friendly_date,
    convert_seconds_to_hours,
)
from data.saved_data import hospital_visits
from datetime import datetime


def calculate_duration(visit):
    """
    Calculates the hours between an admission and a discharge.
    """
    if visit["discharge_date"]:
        discharge_date = generate_friendly_date(visit["discharge_date"])
        length = visit["discharge_date"] - visit["admission_date"]
    else:
        now = datetime.now().timestamp()
        discharge_date = generate_friendly_date(now)
        length = now - visit["admission_date"]
    admission_date = generate_friendly_date(visit["admission_date"])

    return {
        "admission_date": admission_date,
        "discharge_date": discharge_date,
        "length_of_stay": convert_seconds_to_hours(length),
    }


def get_length_of_stay(
    hospital_id, patient_id, many=False, hospital_visits=hospital_visits
):
    """
    Finds a desired patient record and returns the duration of one of their hospital
    visits. If many=True, it returns the duration of all their visits.
    """
    try:
        patient_visits = hospital_visits[hospital_id]["patient_records"][patient_id]
    except KeyError:
        return None
    visits = []
    if many:
        for visit in patient_visits:
            visits.append(calculate_duration(visit))
        return visits

    else:
        visit = patient_visits[-1]
        print(visit)
        return calculate_duration(visit)
