from functions.length_of_stay import get_length_of_stay, calculate_duration
from unittest import TestCase
from data.test_data import hospital_visits


class TestLengthOfStay(TestCase):
    def test_length_of_stay_with_correct_data(self):
        expected_results = {
            "admission_date": "03-09-22",
            "discharge_date": "03-09-22",
            "length_of_stay": "2.3 hours",
        }
        results = get_length_of_stay(
            "HOSP111", "PAT111", hospital_visits=hospital_visits
        )
        self.assertEqual(results, expected_results)
        self.assertEqual(type(results), dict)

    def test_length_of_stay_many_with_correct_data(self):
        results = get_length_of_stay(
            "HOSP111", "PAT111", many=True, hospital_visits=hospital_visits
        )
        self.assertEqual(type(results), list)
        self.assertIsNotNone(results)

    def test_length_of_stay_incorrect_inputs(self):
        self.assertIsNone(get_length_of_stay("invalid_key", 1, hospital_visits))
        with self.assertRaises(TypeError):
            get_length_of_stay()


class TestCalculateDuration(TestCase):
    def test_calculate_duration_with_correct_data(self):
        expected_results_case_1 = {
            "admission_date": "03-09-22",
            "discharge_date": "03-09-22",
            "length_of_stay": "2.3 hours",
        }
        results = calculate_duration(
            {"admission_date": 1662210360.0, "discharge_date": 1662218580.0}
        )
        self.assertEqual(results, expected_results_case_1)
        results = calculate_duration(
            {"admission_date": 1662224820.0, "discharge_date": None}
        )
        self.assertIsNotNone(results["length_of_stay"])

    def test_calculate_duration_with_incorrect_data(self):
        with self.assertRaises(TypeError):
            calculate_duration()
            calculate_duration({"admission_date": None, "discharge_date": None})
