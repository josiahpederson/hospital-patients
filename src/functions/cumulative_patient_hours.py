from data.saved_data import hospital_visits
from datetime import datetime
from .common.time_conversion import convert_seconds_to_hours


def get_cumulative_patient_hours(hospital_visits=hospital_visits):
    """
    Get the cumulative number of hours patients have spent in all the hospitals
    over time
    """
    total_seconds = 0
    for hospital in hospital_visits:
        for patient in hospital_visits[hospital]["patient_records"]:
            patient_records = hospital_visits[hospital]["patient_records"][patient]
            for visit in patient_records:
                if visit["discharge_date"] is not None:
                    total_seconds += visit["discharge_date"] - visit["admission_date"]
                else:
                    now = datetime.now()
                    total_seconds += now.timestamp() - visit["admission_date"]
    return convert_seconds_to_hours(total_seconds)
