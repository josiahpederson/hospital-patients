from functions.current_patient_total import get_current_patient_total
from data.test_data import hospital_visits
from unittest import TestCase, result


class TestCurrentPatientTotal(TestCase):
    def test_current_patient_total_correct_data(self):
        hospital_id = "HOSP111"
        expected_results_1 = {"HOSP111": 2}
        expected_results_2 = {"hospital_totals": [{"HOSP111": 2}, {"HOSP222": 1}]}
        results = get_current_patient_total(
            hospital_id, hospital_visits=hospital_visits
        )
        assert results == expected_results_1
        results = get_current_patient_total(hospital_visits=hospital_visits)
        assert results == expected_results_2

    def test_current_patient_total_incorrect_data(self):
        with self.assertRaises(TypeError):
            get_current_patient_total("HOSP111", None)
            get_current_patient_total(None, "string")
        results = get_current_patient_total(
            "not real hospital", hospital_visits=hospital_visits
        )
        assert results == {"message": "KeyError: Hospital not found"}
