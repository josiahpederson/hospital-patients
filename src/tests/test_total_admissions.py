from unittest import TestCase
from data.test_data import hospital_visits
from functions.total_admissions import get_total_admissions, calculate_total_admissions

class TestTotalAdmissions(TestCase):
    def test_get_total_admissions_with_correct_data(self):
        results = get_total_admissions(hospital_id='HOSP111', hospital_visits=hospital_visits)
        assert results == {'total': 3}
        results = get_total_admissions(hospital_id='HOSP222', hospital_visits=hospital_visits)
        assert results == {'total': 2}

    def get_total_admissions_without_hospital_id(self):
        results = get_total_admissions(hospital_visits=hospital_visits)
        assert results == {'total': 5}

    def test_get_total_admissions_with_incorrect_data(self):
        invalid_hospital = 'HOSP123'
        expected_result = {"message": "KeyError: Hospital not found"}
        results = get_total_admissions(invalid_hospital, hospital_visits)
        self.assertEqual(results, expected_result)
