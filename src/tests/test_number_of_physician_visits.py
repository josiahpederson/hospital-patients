from unittest import TestCase
from data.test_data import physician_visits
from functions.number_of_physician_visits import get_number_of_physician_visits, calculate_number_of_visits

class TestCalculateNumberOfVisits(TestCase):
    def test_calculate_number_of_visits_correct_args(self):
        patient_id = 'PAT555'
        expected_results = [{'date': '01-09-22', 'dispatched_from': 'HOSP111'}]
        results = calculate_number_of_visits(1662051860.0, 1662071860.0, patient_id, physician_visits=physician_visits)
        assert results == expected_results

        results = calculate_number_of_visits(1632041860.0, 1692051860.0, patient_id, physician_visits=physician_visits)
        assert results == [{'date': '01-09-22', 'dispatched_from': 'HOSP111'}]

        results = calculate_number_of_visits(1632041860.0, 1642157360.0, patient_id, physician_visits=physician_visits)
        assert results == []

    def test_calculate_number_of_visits_incorrect_args(self):
        patient_id = 'PAT123'
        results = calculate_number_of_visits(1632041860.0, 1692051860.0, patient_id, physician_visits=physician_visits)
        self.assertIsNone(results)
        with self.assertRaises(TypeError):
            calculate_number_of_visits()
            calculate_number_of_visits('10/12/2022', '11/12/2022', patient_id, physician_visits=physician_visits)


class TestNumberOfPhysicianVisits(TestCase):
    def test_number_of_physician_visits_correct_data(self):
        results = get_number_of_physician_visits('10/12/2022', '11/12/2022', patient_id='PAT555', physician_visits=physician_visits)
        assert results == (0, [])

        results = get_number_of_physician_visits('10/12/2019', '11/12/2023', patient_id='PAT123', physician_visits=physician_visits)
        assert results == {'message': 'No physican visits found for this patient ID.'}

        results = get_number_of_physician_visits('10/12/2019', '11/12/2023', patient_id='PAT555', physician_visits=physician_visits)
        assert results == (1, [{'date': '03-09-22', 'dispatched_from': 'HOSP111'}])

    def test_number_of_physician_visits_incorrect_data(self):
        with self.assertRaises(TypeError):
            get_number_of_physician_visits()

            # improperly formatted date
            get_number_of_physician_visits('10-12-2019', '10-12-2019', patient_id='PAT555', physician_visits=physician_visits)
